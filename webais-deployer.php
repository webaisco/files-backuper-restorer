
<?php
/**
 * WebAIS company
 * http://webais.company
 * Author: Oleg
 * Date: 2.12.2015
 */

const READ_ME = './readme.txt';
const DS = '/';

if (isset($argv[1])) {

    $request = explode('=', $argv[1]);

    switch (strtolower($request[0])) {
        case 'backup':
            $controller = new Backup();
            $controller->init();
            break;
        case 'deploy':
            $controller = new Deploy();
            $controller->init($request);
            break;
        case 'revert':
            $controller = new Revert();
            $controller->init();
            break;
        case '-readme':
            print(file_get_contents(READ_ME));
            break;
        case '-help':
            print("Reference:\n");
            print("* type \"backup\" to create an archive with files that contain in list.txt file\n\n");
            print("* type \"deploy={url}\" to get changes from server by {url}.\r\n");
            print("Note: {url} must contain protocol version: e.g. http://example.com \n\n");
            print("* type \"revert\" to revert changes back after deploy from remote server \n");
            print("WebAIS");
            break;
        default:
            print("Command $request[0] not found");
            break;
    }

} else {
    print("WebAIS backup tool v.1.0\n");
    print("type -help to get reference\n");
    print("type -readme to get description\n");
}

class Directories
{
    const ARCHIVE_NAME = 'backup.tar.gz';
    const ARCHIVE_TAR = 'backup.tar';
    const WEBAIS_DIRECTORY = 'webais-deployer';
    const FILE_LIST = 'list.txt';
    const DEV_DIRECTORY = 'dev';
    const LIVE_DIRECTORY = 'live';

    protected $dir;
    protected $wd;
    protected $errors;
    protected $archive;

    public function init() {

        $this->errors = array('errors' => array(), 'warnings' => array());
        $this->dir = dirname($_SERVER['SCRIPT_NAME']);
        $this->wd = $this->dir . DS . self::WEBAIS_DIRECTORY;
        return $this;
    }

    /**
     * This method create a directory.
     * @param $dir - directory path
     * @param $force - remove file with same name as the $dir
     * @return bool
     */
    protected function mkdirSafe( $dir, $force = false) {

        if (file_exists($dir)) {
            if (is_dir($dir))
                return $dir;
            else if (!$force)
                return false;
            if (unlink($dir))
                print("Removing $dir \n");
        }
        if (mkdir($dir, 0777, true)) {
            print("Creating $dir \n");
            return $dir;
        }
        print("Oops, I can not create the directory $dir. Please create this directory with 0755 permissions to continue working.\n");
        return false;
    }

    /**
     * Reading list.txt with list of all files that we should work with
     * @param $dest - path to directory that contains file list.txt
     * @return array|bool
     */
    protected function readList($dest)
    {
        $list = @fopen($dest . DS . self::FILE_LIST, 'r');
        if (!$list) {
            print("You should fill list.txt file the list of files that should be backed up\n");
            return false;
        }
        $files = array();
        while (($file = fgets($list)) !== false) {
            $file = str_replace(array("\r\n", "\r", "\n"), '', $file);
            $files[] = $file;
        }
        fclose($list);
        return $files;

    }

    /**
     * This method copy files from list.txt.
     * @param $files
     * @param bool $upd - if set to true: create backup files; if set false: overwrite live files
     * @param null $dest - destination folder
     * @param null $src - source folder
     * @param bool $revert - remove files if /dev/ files are new
     */
    protected function copyFilesFromList($files, $upd = false, $dest = null, $src = null, $revert = false)
    {
        $add = true;
        $force = false;
        $_dest = $dest;
        $_src = $src;

        foreach ($files as $file) {

            if ($upd) {
                $dest = $this->wd . DS . $file;
                $src = $this->dir . DS . $file;
            } else {
                $add = false;
                $force = true;
                $dest  = $_dest . DS . $file;
                $src = $_src . DS . $file;
            }

            if (file_exists($src)) {
                $this->copyFolder($src, $dest, $upd, $force, $add);
            } else {
                if ($revert)
                    $this->removeFile($dest);
                else
                    $this->errors['errors'][] = "Error: $file not exists in directory $_src \n";
            }
        }
    }

    /**
     * Copy each folder in directory tree
     * @param $d1 - file source
     * @param $d2 - file destination
     * @param bool $upd
     * @param bool $force
     * @param bool $add
     */
    protected function copyFolder($d1, $d2, $upd = false, $force = true, $add = false) {
        if ( is_dir( $d1 ) ) {
            $d2 = $this->mkdirSafe( $d2, $force );
            if (!$d2) return;
            $d = dir( $d1 );
            while ( false !== ( $entry = $d->read() ) ) {
                if ( $entry != '.' && $entry != '..' )
                    $this->copyFolder( "$d1/$entry", "$d2/$entry", $upd, $force, $add );
            }
            $d->close();
        } else {
            if (!$this->mkdirSafe( dirname($d2), $force )) return;
            $ok = $this->copySafe( $d1, $d2, $force, $add );
            if ($ok)
                $ok = "$d2 - done \n";
            else
                $this->errors['warnings'][] = "Warning: file $d2 is newer and can't be overwritten \n";
            print($ok);
        }
    }

    /**
     * Safely cope file/folder
     * @param $f1 - file/folder source
     * @param $f2 - file/folder destinaton
     * @param $force - overwrite file in any case
     * @param bool $add - add to backup.tar archive (when backup method called)
     * @return bool
     */
    protected function copySafe ($f1, $f2, $force,  $add = false) {

        $time1 = filemtime($f1);

        if (file_exists($f2)) {
            $time2 = filemtime($f2);
            if ($time2 >= $time1 && !$force) return false; // if overwritten file newer than dev/ file, do not overwrite
        }

        $ok = copy($f1, $f2);
        if ($ok) touch($f2, $time1);

        /* $this->archive Phar */
        // add file to backup.tar
        if ($add)
            $this->archive->addFile($f1);


        return $ok;
    }

    /**
     * Print errors or warnings in terminal
     */
    protected function printResult()
    {
        if (!count($this->errors['errors']) && !count($this->errors['warnings'])) {
            print("*** Success ***\n");
        } else {
            $e = count($this->errors['errors']);
            $w = count($this->errors['warnings']);
            print("--Total errors $e, warnings $w:\n\r");
            $i = 1;
            foreach ($this->errors as $type) {
                foreach ($type as $error) {
                    print(" $i. $error");
                    $i++;
                }

            }
            $this->errors['errors'] = array();
            $this->errors['warnings'] = array();
        }
    }

    protected function removeFile($file)
    {
        if (file_exists($file)) {
            (unlink($file)) ? print("$file - removed \n") : $this->errors['errors'][] = "Error: can't remove $file\n";
        }
    }
}

/**
 * Class Backup initialize when called "backup" method
 */
class Backup extends Directories
{
    protected $errors;
    protected $archive;


    /**
     * Create a backup
     */
    public function init()
    {
        parent::init();

        // create a weabais-deployer working directory
        if (!$this->mkdirSafe($this->wd, true)) return;

        $listFile = $this->dir . DS . parent::FILE_LIST;

        // create list.txt if not exist
        if(!file_exists($listFile)) {
            print("Please create the list.txt file and fill in the file the list of files that should be backed up\n");
            return;
        };

        $backup = $this->wd . DS . self::ARCHIVE_TAR;

        $files = $this->readList($this->dir);

        if (!$files || !count($files)) {
            return;
        }

        print("Copying files...\n");

        // remove old backup.tar.gz
        $this->removeFile($this->wd . DS . self::ARCHIVE_NAME);
        $this->archive = new PharData($backup);

        $this->archive->addFile($listFile);

        $this->copyFilesFromList($files, true);

        // compress to .gz
        $this->archive->compress(Phar::GZ);

        // show errors and warnings
        $this->printResult();
    }

}

/**
 * Class Deploy initialize when called "deploy" method
 */
class Deploy extends Directories
{

    /**
     * Apply changes from remote backup file
     * @param $host - remove server, format "http://example.com"
     */
    public function init($host)
    {
        parent::init();

        $archive = $this->wd . DS . parent::ARCHIVE_NAME;
        $archiveTar = $this->wd . DS . parent::ARCHIVE_TAR;

        $dev = $this->wd . DS . self::DEV_DIRECTORY;

        $this->removeFile($archive);

        if (!$this->mkdirSafe($dev, true)) return;

        $url = parse_url($host[1]);
        $host = (isset($url['host'])) ? $url['host'] : $url['path'];
        $paths = explode('\\', $host);
        $file_url = '';
        foreach ($paths as $path) {
            (!$path) ?: $file_url .=  DS . $path;
        }

        (isset($url['scheme'])) ?: $url['scheme'] = 'http';
        $file_url = $url['scheme'] . ':/' . $file_url;
        $file_url .= (isset($url['port'])) ? ':' . $url['port'] : '';
        $file_url .= DS . self::WEBAIS_DIRECTORY . DS . self::ARCHIVE_NAME;

        $this->removeFile($archiveTar);

//        $file_url = "http://paulswarehouse.p.webais.company/backup.tar.gz"; // test url
        copy($file_url, $archive);

        $p = new PharData($archive);
        $p->decompress();

        $phar = new PharData($archiveTar);
        $phar->extractTo($dev, null, true);

        $this->liveDir($dev);

    }

    /**
     * Function working with live and dev directories, create a live files backup and patching live files from server
     * @param $dev - /dev directory
     */
    private function liveDir($dev)
    {

        $dest = $this->wd . DS . self::LIVE_DIRECTORY;
        $src = $this->dir;
        if(!$this->mkdirSafe($dest, true)) return;
        $files = $this->readList($dev);
        if (!$files)
            return;

        print("Copying live files to " . self::WEBAIS_DIRECTORY . DS . self::LIVE_DIRECTORY . "...\n");

        // copy from backup files from current server
        $this->copyFilesFromList($files, false, $dest, $src);

        $this->printResult();

        $src = $this->wd .= DS . self::DEV_DIRECTORY;
        $dest = $this->dir;

        print("Copying received files from " . self::WEBAIS_DIRECTORY . DS . self::DEV_DIRECTORY . "...\n");
        // copy from /dev directory to server
        $this->copyFilesFromList($files, false, $dest, $src);

        $this->printResult();
    }
}

/**
 * Class Revert - revert changes after applying from server
 */
class Revert extends Directories
{
    /**
     * Revert changes on live server
     */
    public function init()
    {
        parent::init();

        $dev = $this->wd . DS . self::DEV_DIRECTORY;
        $revertSource = $this->wd . DS . self::LIVE_DIRECTORY;
        $listPath = $dev;
        $files = $this->readList($listPath);

        // copy from backup folder webais-deployer/live to live server or remove if files in list.txt are new
        $this->copyFilesFromList($files, false, $this->dir, $revertSource, true);

        $this->printResult();
    }
}