 * WebAIS company
 * http://webais.company
 * Author: Oleg
 * Date: 2.12.2015

The script is intended to apply changes made on a dev server to a live server.

Dev Server

1. Upload webais-deployer.php to the root directory of your site.

2. Create the "list.txt" file in the site's root directory and fill in this
    file the list of files and/or folders that needs to be backed up and applied
    to the live web site.

Example of the list.txt

    app/code/local/Namespace/Module/
    app/design/adminhtml/default/default/layout/page.xml
    app/design/adminhtml/default/default/template/catalog/product/page.phtml

3. Run the command "php webais-deployer.php backup" to create an archive
   webais-deployer/backup.tar.gz;

4. Move the archive webais-deployer/backup.tar.gz to your www directory if it is
   different from the site root directory.


Live Server

1. Log in to the live server and change path to your site's root directory
2. Upload webais-deployer.php to the root directory of your site.
3. Run "php webais-deployer.php deploy=http://example.com"
to download changes from the remote dev server.
Updated files will be applied to your live server.
At the same time updated files you can discover in the folder webais-deployer/dev.
Old files from the live server will be located in the folder webais-deployer/live.

Revert Changes

To revert changes back to the initial stateon the live server run the command
 "php webais-deployer.php revert".
